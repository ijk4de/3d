#!/bin/bash
# to run type "bash quaternnion.sh"

#used to create slide show for individual images and have user step through them
# https://ijk4de.gitlab.io/3d/quatpics/  #the webpage for this 

# https://www.w3schools.com/howto/tryit.asp?filename=tryhow_js_slideshow
# https://www.w3schools.com/howto/tryit.asp?filename=tryhow_js_slideshow_auto

currentdir="$(pwd)" #get current directory
fn_quat_export_data="$currentdir/export_data.txt" #file data is stored in
echo "$fn_quat_export_data"

start=1
end=37  #doesn't work if I use 0037... it stops at 0031 .. strange
#for i in {0001..0002}; 
for ((i=$start; i<=$end; i++))
	do 
		#---needs to look like this
		#<div class="mySlides fade">
		#	<div class="numbertext">1 / 3</div>
		#	<img src="quaternion_page_0001.png" style="width:100%">
		#	<div class="text">Caption Text</div>
		#</div>
		#---needs to look like this
		
		i_pad=$(printf %04d $i) #pad varible with zeros
		i_pad_start=$(printf %04d $start) #pad varible with zeros
		i_pad_end=$(printf %04d $end) #pad varible with zeros

		printf '%s' "<div class="\""mySlides fade"\"">"$'\n'\
			"	<div class="\""numbertext"\"">$i_pad / $end</div>"$'\n'\
			"	<img src="\""quaternion_page_${i_pad}.png"\"" style="\""width:100%"\"">"$'\n'\
			"	<div class="\""text"\"">Caption $i_pad / $i_pad_end</div>"$'\n'\
		"</div>"$'\n'\ >> "$fn_quat_export_data"   #put $fn in quotes to deal with spaces in names and dir
done

for ((i=$start; i<=$end; i++))
	do 
		#---needs to look like this
		#<span class="dot" onclick="currentSlide(3)"></span> 
		#---needs to look like this

		i_pad=$(printf %02d $i) #pad varible with zeros
		i_pad_start=$(printf %04d $start) #pad varible with zeros
		i_pad_end=$(printf %04d $end) #pad varible with zeros
		
		printf '%s' "<span class="\""dot"\"" onclick="\""currentSlide($i_pad)"\""></span> "$'\n'\  >>    "$fn_quat_export_data"
done
